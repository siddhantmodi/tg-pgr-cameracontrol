#include "stdio.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <sys/timeb.h>
#include <algorithm>

using namespace std;

#define IMAGE_TIMEOUT 1000 //milliseconds
#define REGULAR_CAMERA_SERIAL_NUMBER 16060624
#define IR_CAMERA_SERIAL_NUMBER 16060608

int main(int argc, char	** argv)
{
	bool function_success;

	PixelFormat pixel_format;
	unsigned int image_width;
	unsigned int image_height;
	ImageFileFormat image_file_format;
	bool save_image;
	string trigger_source_regular;
	string trigger_source_IR;
	parse_command_arguments(argc, argv, &pixel_format, &image_width, &image_height, &image_file_format, &save_image, &trigger_source_regular, &trigger_source_IR);

	// Connect to cameras
	bool is_regular_camera_connected = true;
	cout << "Connecting to the regular camera" << endl;
	Camera regular_camera;
	function_success = connect_to_camera(&regular_camera, REGULAR_CAMERA_SERIAL_NUMBER);
	if (!function_success)
	{
		cout << "Could not connect to regular camera. Will try to connect to IR camera." << endl;
		is_regular_camera_connected = false;
	}

	bool is_IR_camera_connected = true;
	cout << "Connecting to the IR camera" << endl;
	Camera IR_camera;
	function_success = connect_to_camera(&IR_camera, IR_CAMERA_SERIAL_NUMBER);
	if (!function_success)
	{
		if (!is_regular_camera_connected)
		{
			cout << "Could not connect to IR camera either. Exiting the program now.";
			return -1;
		}
		cout << "Could not connect to IR camera. Continuing with only the regular camera." << endl;
		is_IR_camera_connected = false;
	}

	// Set up the cameras
	cout << "Setting up the camera properties" << endl;
	if (is_regular_camera_connected)
	{
		function_success = setup_camera(&regular_camera, PIXEL_FORMAT_RAW8, image_width, image_height);
		if (!function_success)
		{
			cout << "Could not set up the regular camera. Disconnecting camera and exiting." << endl;
			disconnect_camera(&regular_camera);
			return -1;
		}
	}
	if (is_IR_camera_connected)
	{
		function_success = setup_camera(&IR_camera, PIXEL_FORMAT_RAW8, image_width, image_height);
		if (!function_success)
		{
			cout << "Could not set up the IR camera. Disconnecting camera and exiting." << endl;
			disconnect_camera(&IR_camera);
			return -1;
		}
	}

	// Set up the trigger
	if (is_regular_camera_connected)
	{
		cout << "Setting up the triggering features for the regular camera." << endl;
		function_success = set_up_trigger(&regular_camera, trigger_source_regular);
		if (!function_success)
		{
			cout << "Could not set up the trigger settings for the regular camera. Disconnecting camera and exiting." << endl;
			disconnect_camera(&regular_camera);
			return -1; 
		}
	}

	if (is_IR_camera_connected)
	{
		cout << "Setting up the triggering features for the IR camera." << endl;
		function_success = set_up_trigger(&IR_camera, trigger_source_IR);
		if (!function_success)
		{
			cout << "Could not set up the trigger settings for the IR camera. Disconnecting camera and exiting." << endl;
			disconnect_camera(&IR_camera);
			return -1; 
		}
	}

	// Set up the strobe features
	if (is_regular_camera_connected)
	{
		cout << "Setting up the strobe features for the regular camera." << endl;
		function_success = set_up_strobe(&regular_camera);
		if (!function_success)
		{
			cout << "Could not set up the strobe settings for the regular camera. Disconnecting camera and exiting." << endl;
			disconnect_camera(&regular_camera);
			return -1; 
		}
	}

	if (is_IR_camera_connected)
	{
		cout << "Setting up the strobe features for the IR camera." << endl;
		function_success = set_up_strobe(&IR_camera);
		if (!function_success)
		{
			cout << "Could not set up the strobe settings for the IR camera. Disconnecting camera and exiting." << endl;
			disconnect_camera(&IR_camera);
			return -1; 
		}
	}

	// Start the image streaming
	cout << "Starting streaming." << endl;
	if (is_regular_camera_connected)
	{
		function_success = control_camera_streaming(&regular_camera, true);
		if (!function_success)
		{
			cout << "Could not start streaming the regular camera. Disconnecting camera and exiting." << endl;
			disconnect_camera(&regular_camera);
			return -1;
		}
	}
	if (is_IR_camera_connected)
	{
		function_success = control_camera_streaming(&IR_camera, true);
		if (!function_success)
		{
			cout << "Could not start streaming the IR camera. Disconnecting camera and exiting." << endl;
			disconnect_camera(&IR_camera);
			return -1;
		}
	}

	// Infinite while loop should start here - receives command from python code to check for trigger ready
	// If trigger is ready, camera gets triggered, image is grabbed and saved or passed into stdout, and then dropped
	// In this loop, any commands from stdin are grabbed and the appropriate action is taken
	string input_command;
	unsigned int regular_image_index = 0;
	unsigned int IR_image_index = 0;
	cin >> input_command;
	while (input_command.compare("exit") != 0)
	{
		if (input_command.compare("grab") == 0)
		{
			if (!is_regular_camera_connected)
			{
				cout << "Regular camera is not connected. Enter 'grabir' to grab an image with the IR camera." << endl;
				cin >> input_command;
				continue;
			}
			if (save_image)
			{
				function_success = grab_and_save_image(&regular_camera, image_file_format, &regular_image_index, true);
				if (!function_success)
				{
					cout << "Could not grab and save regular image." << endl;
				}
			}
			else
			{
				function_success = grab_and_send_image_to_stdout(&regular_camera, &regular_image_index, true);
				if (!function_success)
				{
					cout << "Could not grab and send image to stdout." << endl;
				}
			}
		}
		else if (input_command.compare("grabir") == 0)
		{
			if (!is_IR_camera_connected)
			{
				cout << "IR camera is not connected. Enter 'grab' to grab an image with the regular camera." << endl;
				cin >> input_command;
				continue;
			}
			if (save_image)
			{
				function_success = grab_and_save_image(&IR_camera, image_file_format, &IR_image_index, false);
				if (!function_success)
				{
					cout << "Could not grab and save IR image." << endl;
				}
			}
			else
			{
				function_success = grab_and_send_image_to_stdout(&IR_camera, &IR_image_index, false);
				if (!function_success)
				{
					cout << "Could not grab and send image to stdout." << endl;
				}
			}
		}
		else if (strstr(input_command.c_str(), "exposureir"))
		{
			if (is_IR_camera_connected)
			{
				double exposure_time_to_set;
				scanf("%lf", &exposure_time_to_set);
				function_success = set_exposure_time(&IR_camera, exposure_time_to_set);
				if (!function_success)
				{
					cout << "Could not set exposure time to " << exposure_time_to_set << "." << endl;
				}
			}
			else
			{
				cout << "IR camera is not connected. Use compensation command." << endl;
			}
		}
		else if (strstr(input_command.c_str(), "compensationir"))
		{
			if (is_IR_camera_connected)
			{
				double compensation_to_set;
				scanf("%lf", &compensation_to_set);
				function_success = set_exposure_compensation(&IR_camera, compensation_to_set);
				if (!function_success)
				{
					cout << "Could not set exposure comepensation to " << compensation_to_set << "." << endl;
				}
			}
			else
			{
				cout << "IR camera is not connected. Use compensation command." << endl;
			}
		}
		else if (strstr(input_command.c_str(), "exposure"))
		{
			if (is_regular_camera_connected)
			{
				double exposure_time_to_set;
				scanf("%lf", &exposure_time_to_set);
				function_success = set_exposure_time(&regular_camera, exposure_time_to_set);
				if (!function_success)
				{
					cout << "Could not set exposure time to " << exposure_time_to_set << "." << endl;
				}
			}
			else
			{
				cout << "Regular camera is not connected. Use exposureir command." << endl;
			}
		}
		else if (strstr(input_command.c_str(), "compensation"))
		{
			if (is_regular_camera_connected)
			{
				double compensation_to_set;
				scanf("%lf", &compensation_to_set);
				function_success = set_exposure_compensation(&regular_camera, compensation_to_set);
				if (!function_success)
				{
					cout << "Could not set exposure comepensation to " << compensation_to_set << "." << endl;
				}
			}
			else
			{
				cout << "Regular camera is not connected. Use compensationir command." << endl;
			}
		}
		else if (input_command.compare("trigger") == 0)
		{
			if (is_regular_camera_connected)
			{
				if (trigger_source_regular == "software")
				{
					function_success = fire_software_trigger(&regular_camera);
				}
				if (!function_success)
				{
					cout << "Could not fire software trigger." << endl;
				}
			}
			else
			{
				cout << "Regular camera is not connected. Use triggerir command." << endl;
			}
		}
		else if (input_command.compare("triggerir") == 0)
		{
			if (is_IR_camera_connected)
			{
				if (trigger_source_IR == "software")
				{
					function_success = fire_software_trigger(&IR_camera);
				}
				if (!function_success)
				{
					cout << "Could not fire software trigger." << endl;
				}
			}
			else
			{
				cout << "Regular camera is not connected. Use trigger command." << endl;
			}
		}
		else
		{
			cout << input_command << " is an invalid command. Choose from the following options: " << endl;
			cout << "trigger" << endl;
			cout << "triggerir" << endl;
			cout << "grab" << endl;
			cout << "grabir" << endl;
			cout << "exposure xx.xx" << endl;
			cout << "exposureir xx.xx" << endl;
			cout << "compensation xx.xx" << endl;
			cout << "compensationir xx.xx" << endl;
			cout << "exit" << endl;
		}
		cin >> input_command;
	}

	// Disconnect cameras and stop streams
	if (is_regular_camera_connected)
	{
		cout << "Stopping streaming and disconnecting regular camera." << endl;
		function_success = control_camera_streaming(&regular_camera, false);
		if (!function_success)
		{
			cout << "Could not stop capture. Disconnecting from camera." << endl;
		}
		function_success = disconnect_camera(&regular_camera);
		if (!function_success)
		{
			cout << "Could not disconnect from camera." << endl;
			return -1;
		}
	}
	if (is_IR_camera_connected)
	{
		cout << "Stopping streaming and disconnecting IR camera." << endl;
		function_success = control_camera_streaming(&IR_camera, false);
		if (!function_success)
		{
			cout << "Could not stop capture. Disconnecting from camera." << endl;
		}
		function_success = disconnect_camera(&IR_camera);
		if (!function_success)
		{
			cout << "Could not disconnect from camera." << endl;
			return -1;
		}
	}
	return 0;
}

/******************************FUNCTION DEFINITIONS******************************/

void PrintError(Error error)
{
	error.PrintErrorTrace();
}

bool connect_to_camera(Camera* cam, unsigned int serial_number)
{
	Error error;

	BusManager busMgr;
	unsigned int numCameras;
	error = busMgr.GetNumOfCameras(&numCameras);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	cout << "Number of cameras detected: " << numCameras << endl;

	if (numCameras < 1)
	{
		cout << "Insufficient number of cameras... exiting" << endl;
		return false;
	}

	PGRGuid guid;
	error = busMgr.GetCameraFromSerialNumber(serial_number, &guid);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	// Connect to a camera
	error = cam->Connect(&guid);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	// successful
	return true;
}

bool setup_camera(Camera* cam, PixelFormat pixel_format, unsigned int width, unsigned int height)
{
	// Get the camera information
	CameraInfo camInfo;
	Error error = cam->GetCameraInfo(&camInfo);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	// Query for available Format 7 modes
	Format7Info fmt7Info;
	bool supported;
	fmt7Info.mode = MODE_0;
	error = cam->GetFormat7Info(&fmt7Info, &supported);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	if ((pixel_format & fmt7Info.pixelFormatBitField) == 0)
	{
		// Pixel format not supported!
		cout << "Pixel format is not supported" << endl;
		return false;
	}

	Format7ImageSettings fmt7ImageSettings;
	fmt7ImageSettings.mode = MODE_0;
	fmt7ImageSettings.offsetX = 0;
	fmt7ImageSettings.offsetY = 0;
	fmt7ImageSettings.pixelFormat = pixel_format;
	if (width > fmt7Info.maxWidth)
	{
		fmt7ImageSettings.width = fmt7Info.maxWidth;
	}
	else
	{
		fmt7ImageSettings.width = width / fmt7Info.imageHStepSize * fmt7Info.imageHStepSize;
	}
	
	if (height > fmt7Info.maxHeight)
	{
		fmt7ImageSettings.height = fmt7Info.maxHeight;
	}
	else
	{
		fmt7ImageSettings.height = height / fmt7Info.imageVStepSize * fmt7Info.imageVStepSize;
	}

	bool valid;
	Format7PacketInfo fmt7PacketInfo;

	// Validate the settings to make sure that they are valid
	error = cam->ValidateFormat7Settings(
		&fmt7ImageSettings,
		&valid,
		&fmt7PacketInfo);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	if (!valid)
	{
		// Settings are not valid
		cout << "Format7 settings are not valid" << endl;
		return false;
	}

	// Set the settings to the camera
	error = cam->SetFormat7Configuration(
		&fmt7ImageSettings,
		fmt7PacketInfo.recommendedBytesPerPacket);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	// Get the camera configuration
	FC2Config config;
	error = cam->GetConfiguration(&config);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	// Set the grab timeout to 0.5 seconds
	config.grabTimeout = IMAGE_TIMEOUT;

	// Set the camera configuration
	error = cam->SetConfiguration(&config);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	EmbeddedImageInfo embedded_info;
	embedded_info.shutter.onOff = true;
	embedded_info.gain.onOff = true;
	embedded_info.exposure.onOff = true;
	error = cam->SetEmbeddedImageInfo(&embedded_info);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	return true;
}

bool control_camera_streaming(Camera* cam, bool stream_state)
{
	// Start the stream
	Error error;
	if (stream_state)
	{
		error = cam->StartCapture();
	}
	else
	{
		error = cam->StopCapture();
	}

	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	return true;
}

bool disconnect_camera(Camera* cam)
{
	// Disconnect the camera
	Error error = cam->Disconnect();
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	return true;
}

bool set_exposure_time(Camera* cam, double exposure_time_value)
{
	Property exposure_time;
	exposure_time.type = SHUTTER;
	Error error = cam->GetProperty(&exposure_time);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	PropertyInfo exposure_time_info;
	exposure_time_info.type = SHUTTER;
	error = cam->GetPropertyInfo(&exposure_time_info);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}
	if (exposure_time_value < exposure_time_info.absMin || exposure_time_value > exposure_time_info.absMax)
	{
		cout << "The exposure time must be between " << exposure_time_info.absMin << " and "
			<< exposure_time_info.absMax << " ms. ";
		return false;
	}

	exposure_time.autoManualMode = false;
	exposure_time.absValue = exposure_time_value;
	error = cam->SetProperty(&exposure_time);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	return true;
}

bool set_exposure_compensation(Camera* cam, double compensation_value)
{
	Property auto_exposure;
	auto_exposure.type = AUTO_EXPOSURE;
	Error error = cam->GetProperty(&auto_exposure);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	PropertyInfo auto_exposure_info;
	auto_exposure_info.type = AUTO_EXPOSURE;
	error = cam->GetPropertyInfo(&auto_exposure_info);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}
	if (compensation_value < auto_exposure_info.absMin || compensation_value > auto_exposure_info.absMax)
	{
		cout << "The gain must be between " << auto_exposure_info.absMin << " and " << auto_exposure_info.absMax << " %. ";
		return false;
	}

	auto_exposure.autoManualMode = false;
	auto_exposure.absValue = compensation_value;
	error = cam->SetProperty(&auto_exposure);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	return true;
}

bool set_up_trigger(Camera* cam, string trigger_source)
{
	if (trigger_source == "stdin")
	{

		// Disable the trigger
		TriggerMode triggerMode;
		triggerMode.onOff = false;
		Error error = cam->SetTriggerMode(&triggerMode);
		if (error != PGRERROR_OK)
		{
			PrintError(error);
			return false;
		}
	}
	else
	{
		// Get current trigger settings
		TriggerMode triggerMode;
		Error error = cam->GetTriggerMode(&triggerMode);
		if (error != PGRERROR_OK)
		{
			PrintError(error);
			return false;
		}

		// Set camera to trigger mode 0 with GPIO Pin2
		triggerMode.onOff = true;
		triggerMode.mode = 0;
		triggerMode.parameter = 0;
		triggerMode.polarity = 1;
		if (trigger_source == "gpio")
		{
			triggerMode.source = 2;
		}
		else
		{
			triggerMode.source = 7;
		}

		error = cam->SetTriggerMode(&triggerMode);
		if (error != PGRERROR_OK)
		{
			PrintError(error);
			return false;
		}
	}

	return true;
}

bool poll_for_trigger_ready(Camera* cam)
{
	const unsigned int register_software_trigger = 0x62C;
	Error error;
	unsigned int register_value = 0;

	do
	{
		error = cam->ReadRegister(register_software_trigger, &register_value);
		if (error != PGRERROR_OK)
		{
			PrintError(error);
			return false;
		}

	} while ((register_value >> 31) != 0);

	return true;
}

bool fire_software_trigger(Camera* cam)
{
	poll_for_trigger_ready(cam);
	const unsigned int register_software_trigger = 0x62C;
	const unsigned int software_trigger_fire_value = 0x80000000;
	Error error;

	error = cam->WriteRegister(register_software_trigger, software_trigger_fire_value);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	return true;
}

bool set_up_strobe(Camera* cam)
{
	// Turn off Strobing on GPIO 1
	StrobeControl strobe_control;
	strobe_control.source = 1;
	Error error = cam->GetStrobe(&strobe_control);
	strobe_control.onOff = false;
	error = cam->SetStrobe(&strobe_control);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	// Enable strobing on GPIO 3
	strobe_control.source = 3;
	error = cam->GetStrobe(&strobe_control);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}
	strobe_control.onOff = true;
	strobe_control.delay = 0;
	strobe_control.duration = 0;
	strobe_control.polarity = 1;
	error = cam->SetStrobe(&strobe_control);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}

	return true;
}

bool grab_and_save_image(Camera* cam, ImageFileFormat image_format, unsigned int* image_index, bool is_regular_camera)
{
	// Grab and save image
	Image image;
	Error error = cam->RetrieveBuffer(&image);
	ImageMetadata meta_data = image.GetMetadata();
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}
	ostringstream image_name;
	image_name << "Image_";
	if (is_regular_camera)
	{
		image_name << "Reg_" << *image_index;;
	}
	else
	{
		image_name << "IR_" << *image_index;;
	}
	if (image_format == JPEG)
	{
		image_name << ".jpg";
	}
	Image converted_image;
	converted_image.SetColorProcessing(HQ_LINEAR);
	image.Convert(PIXEL_FORMAT_RGB8, &converted_image);
	converted_image.Save(image_name.str().c_str(), image_format);
	cout << image_name.str() << endl;
	image.ReleaseBuffer();
	*image_index = *image_index + 1;
	return true;
}

bool grab_and_send_image_to_stdout(Camera* cam, unsigned int* image_index, bool is_regular_camera)
{
	// Grab image
	Image image;
	Error error = cam->RetrieveBuffer(&image);
	if (error != PGRERROR_OK)
	{
		PrintError(error);
		return false;
	}
	ostringstream image_name;
	image_name << "Image_";
	if (is_regular_camera)
	{
		image_name << "Reg_" << *image_index;;
	}
	else
	{
		image_name << "IR_" << *image_index;;
	}
	cout << image.GetDataSize() << image_name.str() << image.GetData() << "EndImage";
	image.ReleaseBuffer();
	*image_index = *image_index + 1;
	return true;
}

void parse_command_arguments(int argc, char** argv, PixelFormat* pixel_format, unsigned int* width, 
	unsigned int* height, ImageFileFormat* image_format, bool* save_image, string* trigger_source_regular, string* trigger_source_IR)
{
	string pixel_format_command_string = "--pixelformat";
	string RGB8_command_string = "RGB8";
	string YUV444_command_string = "YUV444";
	string width_command_string = "--width";
	string height_command_string = "--height";
	string image_format_command_string = "--imageformat";
	string JPEG_command_string = "jpeg";
	string RAW_command_string = "raw";
	string image_grab_method_command_string = "--imagegrab";
	string save_command_string = "save";
	string stdout_command_string = "stdout";
	string regular_camera_grab_mode_command_string = "--regulargrabmode";
	string IR_camera_grab_mode_command_string = "--irgrabmode";
	string stdin_grab_mode_command_string = "stdin";
	string gpio_grab_mode_command_string = "gpio";
	string software_trigger_grab_mode_command_string = "software";


	for (int i = 1; i < argc; i++)
	{
		string temp_arg = argv[i];
		transform(temp_arg.begin(), temp_arg.end(), temp_arg.begin(), ::tolower);
		if (!strcmp(temp_arg.c_str(), pixel_format_command_string.c_str()))
		{
			string temp_pixel_format = argv[++i];
			if (temp_pixel_format == YUV444_command_string)
			{
				*pixel_format = PIXEL_FORMAT_444YUV8;
			}
			else
			{
				*pixel_format = PIXEL_FORMAT_RGB8;
			}
		}
		else if (!strcmp(temp_arg.c_str(), width_command_string.c_str()))
		{
			*width = atoi(argv[++i]);
		}
		else if (!strcmp(temp_arg.c_str(), height_command_string.c_str()))
		{
			*height = atoi(argv[++i]);
		}
		else if (!strcmp(temp_arg.c_str(), image_format_command_string.c_str()))
		{
			string temp_image_format = argv[++i];
			transform(temp_image_format.begin(), temp_image_format.end(), temp_image_format.begin(), ::tolower);
			if (temp_image_format == RAW_command_string)
			{
				*image_format = RAW;
			}
			else
			{
				*image_format = JPEG;
			}
		}
		else if (!strcmp(temp_arg.c_str(), image_grab_method_command_string.c_str()))
		{
			string temp_grab_method = argv[++i];
			transform(temp_grab_method.begin(), temp_grab_method.end(), temp_grab_method.begin(), ::tolower);
			if (temp_grab_method == stdout_command_string)
			{
				*save_image = false;
			}
			else
			{
				*save_image = true;
			}
		}
		else if (!strcmp(temp_arg.c_str(), regular_camera_grab_mode_command_string.c_str()))
		{
			string temp_grab_mode = argv[++i];
			transform(temp_grab_mode.begin(), temp_grab_mode.end(), temp_grab_mode.begin(), ::tolower);
			if (temp_grab_mode == gpio_grab_mode_command_string)
			{
				*trigger_source_regular = "gpio";
			}
			else if (temp_grab_mode == software_trigger_grab_mode_command_string)
			{
				*trigger_source_regular = "software";
			}
			else
			{
				*trigger_source_regular = "stdin";
			}
		}
		else if (!strcmp(temp_arg.c_str(), IR_camera_grab_mode_command_string.c_str()))
		{
			string temp_grab_mode = argv[++i];
			transform(temp_grab_mode.begin(), temp_grab_mode.end(), temp_grab_mode.begin(), ::tolower);
			if (temp_grab_mode == gpio_grab_mode_command_string)
			{
				*trigger_source_IR = "gpio";
			}
			else if (temp_grab_mode == software_trigger_grab_mode_command_string)
			{
				*trigger_source_IR = "software";
			}
			else
			{
				*trigger_source_IR = "stdin";
			}
		}
		else
		{
			cout << "Extra/Invalid command line argument: " << argv[i] << endl;
		}
	}
}