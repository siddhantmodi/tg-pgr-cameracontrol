#### Team Guardian PointGrey camera control software
This sofware is based on [Point Grey's](https://www.ptgrey.com) FlyCapture2 API to control their machine vision cameras. [Team Guardian](http://www.teamguardian.ca/) uses two CM3-U3-50S5C-CS cameras on board the fixed-wing UAV for geographical surveying.
