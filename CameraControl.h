/* This file is a dependency for CameraControl.cpp */

#include "FlyCapture2.h"
using namespace FlyCapture2;

// This is an API function to print out camera errors
void PrintError(Error error);

// This function connects to the cameras with the serial numbers taken from command arguments
bool connect_to_camera(Camera* cam, unsigned int serial_number);

// This functions sets up the basic camera parameters and gets it ready for use
bool setup_camera(Camera* cam, PixelFormat pixel_format, unsigned int width, unsigned int height);

// This function is used to turn on/off the camera image stream
bool control_camera_streaming(Camera* cam, bool stream_state);

// This function is used to disconnect from the camera(s)
bool disconnect_camera(Camera* cam);

// This function is used to set the exposure time of the camera
bool set_exposure_time(Camera* cam, double exposure_time_value);

// This function is used to modify the exposure compensation parameter of the camera
bool set_exposure_compensation(Camera* cam, double compensation_value);

// This functions is used to set up the GPIO/software trigger capabilities of the camera
bool set_up_trigger(Camera* cam, string trigger_source);

// This function checks if the camera is ready to be triggered
bool poll_for_trigger_ready(Camera* cam);

// This function is used to fire a software trigger
bool fire_software_trigger(Camera* cam);

// This function is used to set up the strobe feature of the camera - the camera outputs a pulse with a width of exposure time via GPIO upon every image capture
bool set_up_strobe(Camera* cam);

// This function is used to grab an image from the buffer and save it to memory
bool grab_and_save_image(Camera* cam, ImageFileFormat image_format, unsigned int* image_index, bool is_regular_camera);

// This function is used to grab an image from the buffer and send it to stdout as a bitmap (array of chars)
bool grab_and_send_image_to_stdout(Camera* cam, unsigned int* image_index, bool is_regular_camera);

// This function is used to parse out the arguments that are entered in the command line when the program is being started
void parse_command_arguments(int argc, char** argv, PixelFormat* pixel_format, unsigned int* width, 
	unsigned int* height, ImageFileFormat* image_format, bool* save_image, string* trigger_source_regular, string* trigger_source_IR);

